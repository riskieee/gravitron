const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");



module.exports = {
  entry: {
    bundle: './src/index.js'
  },
  mode: 'development',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'index.js'
  },
  module: {
    rules: [
//    {
//      enforce: 'pre',
//      test: /\.js$/,
//      exclude: /node_modules/,
//      use: 'eslint-loader',
//      options: {
//              formatter: require("eslint/lib/formatters/stylish")
//            }
//    }, 
    {
      test: /\.js$/,
      exclude: /node_modules/,
     use: 'babel-loader'
    }, {
      test: /\.scss$/,
      use: [{
          loader: "style-loader"
      }, {
          loader: "css-loader", options: {
              sourceMap: true
          }
      }, {
          loader: "sass-loader", options: {
              sourceMap: true
          }
      }]
    }]
  },
  plugins: [
    new HtmlWebpackPlugin({
        title: 'graviTRON',
        meta: {
          charset:'utf-8',
          viewport: 'width=device-width, initial-scale=1',
          description:'toolchain is node, npm, webpack, jslint, babel es6, scss, generated html, scripts lint-build-server'
        }
      }),
    new UglifyJsPlugin()
  ]
};
