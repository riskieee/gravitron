## graviTRON

**play by throwing this in your browser:**
> ``` ./dist/index.html```


**for remix and rebuild**
install https://nodejs.org/en/


**get dependencies:**  
> ``` npm install```

**use webpack scripts for files from ./src/ files to ./src/**** 
> **jslint:**  ```npm run lint```

> **build:**  ```npm run build```

> **server on localhost:8080:** ```npm run server```
