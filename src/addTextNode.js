// default js: addTextNode('Text to add','p','containerid2add');
// min js: addTextNode('Text to add');
// if no container id is given html will be created: <div id="containerid2add"></div>

// main app
function addTextNode(
    text2add = 'Text is missing!',
    tag2add = 'p',
    where2addID = 'containerid2add'
  ){

  // check for container and create if missing
  if(!document.getElementById(where2addID)){
    let div2Add = document.createElement('div');
    div2Add.id = 'containerid2add';
    document.body.appendChild(div2Add);
    console.log('container created');
  }
  // create tag
  let newtag2add = document.createElement(tag2add);
  let textnode2add = document.createTextNode(text2add);
  newtag2add.appendChild(textnode2add);

  // add to container
  let element2add = document.getElementById(where2addID);
  element2add.appendChild(newtag2add);
}

// export addTextNode module
export {addTextNode};
