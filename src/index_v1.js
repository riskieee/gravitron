// ES6 imported CSS
import './style.scss';

// display renderer for setup
    class Renderer{
      // construct object recipe
      constructor(e){
        this.e = e;
        this.setup();
      }

      // methods of "this" object
      setup(){
        let score = document.getElementById("score");
        this.score = score;
        let ball = document.createElement("div");
        ball.setAttribute("id", "ball");
        this.e.appendChild(ball);
        this.ball = ball;
      }
      // exernal API for position change
      render(position, scoreCounter){
        this.ball.style.top = position + "px";
        this.score.innerHTML = scoreCounter;
      }
    }
// falling ball
    class Player{
      constructor(){
        this.position = 0;
        this.speed = 0;
      }
      fallLoop(){
        this.speed = this.speed + 0.01;
        this.position = this.position + this.speed;
      }
      upLoop(){
        this.speed = this.speed  - 0.5 ;
      }
    }

// game controller
    class Game {
      constructor(e){
        this.renderer = new Renderer(e);
        this.player = new Player;
        this.e = e;
        this.isRunning = true;
        this.setup();
      }
      setup(){
        if (this.isRunning){
          this.e.addEventListener("click", () => {
            this.player.upLoop();
          }, false);
        }
      }
      restart(){
        if (!this.isRunning){
          this.e.addEventListener("click", () => {
            // console.log('restart clicked');
            // this.isRunning = true;
            // this.setup();
            window.location.reload(); // dirtyfix
          }, false);
        }
      }
      start(){
        let counter = 0;
        let timer = setInterval(() => {
          counter++;
          this.player.fallLoop();
          if (this.player.position < 0 ||
              this.player.position > this.e.clientHeight - 30 ){
            this.isRunning = false;
            clearInterval(timer);
            this.e.innerHTML = 'GAME<br>OVER<br>&nbsp;<br>restart?';
            this.restart();
          }
          if (this.isRunning){
            this.renderer.render(this.player.position, counter);
          }

        }, 8);
      }
    }

    let gameHeader = document.createElement("h1");
    gameHeader.innerHTML = 'gravi<strong>TRON</strong>';
    document.body.appendChild(gameHeader);

    let gameContainer = document.createElement("div");
    gameContainer.setAttribute("id", "graviTRON");
    document.body.appendChild(gameContainer);

    let gameInfo = document.createElement("p");
    gameInfo.innerHTML = '<strong>Click the box</strong> for a highscore!';
    document.body.appendChild(gameInfo);

    let gameScore = document.createElement("p");
    gameScore.setAttribute("id", "score");
    gameScore.innerHTML = '00000';
    document.body.appendChild(gameScore);

    let graviTRONGame = new Game(document.getElementById("graviTRON"));
    graviTRONGame.start();
