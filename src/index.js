'use strict;'

// ES6 imported CSS
import './style.scss';


// game controller + constructor
class Game {
  constructor(){
    // create stage
    this.createStage('h1','gravi<strong>TRON</strong>');
    this.createStage('div', '', 'graviTRON');
    this.createStage('p','<strong>Click the box</strong> for a highscore!');
    this.createStage('p','00000','score');
    let e = document.getElementById("graviTRON");
    this.e = e;

    // start game
    this.renderer = new Renderer(e);
    this.player = new Player;
    this.isRunning = true;
    this.setup();
  }
  setup(){
    if (this.isRunning){
      this.e.addEventListener("click", () => {
        this.player.upLoop();
      }, false);
    }
  }
  restart(){
    if (!this.isRunning){
          this.e.addEventListener("click", () => {
            window.location.reload(); // dirtyfix
          }, false);
        }
  }
  start(){
    let counter = 0;
    let timer = setInterval(() => {
      counter++;
      this.player.fallLoop();
      if (this.player.position < 0 ||
          this.player.position > this.e.clientHeight - 30 ){
        this.isRunning = false;
        clearInterval(timer);
        this.e.innerHTML = 'GAME<br>OVER<br>&nbsp;<br>restart?';
        this.restart();
      }
      if (this.isRunning){
        this.renderer.render(this.player.position, counter);
        }

    }, 8);
  }
  createStage(
    tag2add = 'p',
    text2add = '',
    id2add = ''
  ){
    // create tag
    let newtag2add = document.createElement(tag2add);
        newtag2add.innerHTML = text2add;
        if(id2add){
          newtag2add.setAttribute("id", id2add);
        }
        // add to body
        document.body.appendChild(newtag2add);
    }
}

// display renderer for setup
class Renderer{
  // construct object recipe
  constructor(e){
    this.e = e;
    this.setup();
  }

  // methods of "this" object
  setup(){
    let score = document.getElementById("score");
    this.score = score;
    let ball = document.createElement("div");
    ball.setAttribute("id", "ball");
    this.e.appendChild(ball);
    this.ball = ball;
  }
  // exernal API for position change
  render(position, scoreCounter){
    this.ball.style.top = position + "px";
    this.score.innerHTML = scoreCounter;
  }
}
// falling ball
class Player{
  constructor(){
    this.position = 0;
    this.speed = 0;
  }
  fallLoop(){
    this.speed = this.speed + 0.01;
    this.position = this.position + this.speed;
  }
  upLoop(){
    this.speed = this.speed  - 0.5 ;
  }
}




// gameStage.create();
let graviTRONGame = new Game();
graviTRONGame.start();
